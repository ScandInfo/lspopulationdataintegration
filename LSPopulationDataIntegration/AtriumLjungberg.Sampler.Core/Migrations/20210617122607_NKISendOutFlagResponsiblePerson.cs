﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AtriumLjungberg.Sampler.Core.Migrations
{
    public partial class NKISendOutFlagResponsiblePerson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasNKIFlagEnabled",
                table: "ResponsiblePersons",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasNKIFlagEnabled",
                table: "ResponsiblePersons");
        }
    }
}
