﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AtriumLjungberg.Sampler.Core.Migrations
{
    public partial class SampleColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastSurveySendOut",
                table: "Samples");

            migrationBuilder.AddColumn<bool>(
                name: "IsHandled",
                table: "Samples",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "RespondentId",
                table: "Samples",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RespondentPassword",
                table: "Samples",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "SurveySent",
                table: "Samples",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MailQueueItems",
                columns: table => new
                {
                    MailQueueItemId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RespondentId = table.Column<int>(type: "int", nullable: false),
                    RespondentPassword = table.Column<int>(type: "int", nullable: false),
                    StreetAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MailQueueItemType = table.Column<int>(type: "int", nullable: false),
                    SampleId = table.Column<int>(type: "int", nullable: false),
                    SendMailAfter = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsHandled = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailQueueItems", x => x.MailQueueItemId);
                    table.ForeignKey(
                        name: "FK_MailQueueItems_Samples_SampleId",
                        column: x => x.SampleId,
                        principalTable: "Samples",
                        principalColumn: "SampleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MailQueueItems_SampleId",
                table: "MailQueueItems",
                column: "SampleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MailQueueItems");

            migrationBuilder.DropColumn(
                name: "IsHandled",
                table: "Samples");

            migrationBuilder.DropColumn(
                name: "RespondentId",
                table: "Samples");

            migrationBuilder.DropColumn(
                name: "RespondentPassword",
                table: "Samples");

            migrationBuilder.DropColumn(
                name: "SurveySent",
                table: "Samples");

            migrationBuilder.AddColumn<DateTime>(
                name: "LastSurveySendOut",
                table: "Samples",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
