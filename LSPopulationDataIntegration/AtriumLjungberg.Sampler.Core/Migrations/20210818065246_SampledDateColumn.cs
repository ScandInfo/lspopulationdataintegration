﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AtriumLjungberg.Sampler.Core.Migrations
{
    public partial class SampledDateColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SurveySent",
                table: "Samples",
                newName: "SampledDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SampledDate",
                table: "Samples",
                newName: "SurveySent");
        }
    }
}
