﻿using AtriumLjungberg.Sampler.Core.Entities;
using LS.PopulationData.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Data
{
    public class ALDbContext : DbContext
    {
        public ALDbContext(DbContextOptions<ALDbContext> options) 
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Company> Companies{ get; set; }
        public DbSet<ResponsiblePerson> ResponsiblePersons { get; set; }
        public DbSet<Sample> Samples { get; set; }
        public DbSet<MailQueueItem> MailQueueItems { get; set; }


    }
}
