﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtriumLjungberg.Sampler.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace AtriumLjungberg.Sampler.Core.Data
{
    public class NebuContext : DbContext
    {
        public NebuContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<NebuSample> Samples { get; set; }
        public DbSet<NebuFillingN> FillingN{ get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NebuSample>()
                .HasKey(n => n.Code);

            modelBuilder.Entity<NebuSample>()
                .Property(p => p.Status)
                .HasColumnType("SMALLINT");

            modelBuilder.Entity<NebuFillingN>()
                .HasKey(n => n.CallCode);

        }
    }
}
