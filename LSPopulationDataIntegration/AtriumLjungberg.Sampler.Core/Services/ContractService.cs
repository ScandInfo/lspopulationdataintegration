﻿
using AtriumLjungberg.Sampler.Core.Data;
using AtriumLjungberg.Sampler.Core.Entities;
using LS.PopulationData.Core.DTO;
using LS.PopulationData.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace LS.PopulationData.Core.Services
{
    public class ContractService : IContractService
    {
        private ALDbContext _dbContext;

        public ContractService(ALDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task UpsertSamplesAsync(List<APIModelDTO> data)
        {
            var grouped = data.GroupBy(g => g.object_no).ToDictionary(g => g.Key, g => g.ToList());

            foreach (var item in grouped)
            {
                var firstItem = item.Value.FirstOrDefault();
                if (firstItem?.person_email is null)
                {
                    continue;
                }
                Company company = await AddOrGetCompanyAsync(firstItem.company_name);
                Contract contract = await UpsertContractAsync(firstItem, company);
                SetContractResponsiblePersons(item.Value, contract);

                await _dbContext.SaveChangesAsync();
            }
        }

        private static void SetContractResponsiblePersons(List<APIModelDTO> apiModels, Contract contract)
        {
            contract.ResponsiblePersons.Clear();

            var persons = apiModels.Select(person => 
                new ResponsiblePerson
                {
                    ResponsiblePersonEmail = person.person_email, 
                    ResponsiblePersonName = person.person_name, 
                    HasNKIFlagEnabled = person.person_sendouts.Contains("NKI")
                }).ToList();
            contract.ResponsiblePersons = persons;
        }

        private async Task<Contract> UpsertContractAsync(APIModelDTO apiModel, Company company)
        {
            var contract = await _dbContext.Contracts.FirstOrDefaultAsync(c => c.ObjectNo.ToUpper().Equals(apiModel.object_no.ToUpper()));

            if (contract is not null)
            {
                contract.Status = apiModel.contract_contractstatus;
                contract.Company = company;
                contract.ObjectType = apiModel.contract_objecttype;
                contract.PropertyBusinessArea = apiModel.businessarea_area;
                contract.PropertyId = apiModel.property_propertyid;
                contract.StreetAddress = apiModel.object_address;
                contract.PostalCity = apiModel.property_postalcity;
                contract.FromDate = apiModel.contract_fromdate;
                contract.ToDate = apiModel.contract_todate;

                return contract;
            }

            contract = new Contract
            {
                Company = company,
                FromDate = apiModel.contract_fromdate,
                ToDate = apiModel.contract_todate,
                ObjectNo = apiModel.object_no,
                ObjectType = apiModel.contract_objecttype,
                PostalCity = apiModel.property_postalcity,
                PropertyBusinessArea = apiModel.businessarea_area,
                PropertyId = apiModel.property_propertyid,
                Status = apiModel.contract_contractstatus,
                StreetAddress = apiModel.object_address,
            };
            await _dbContext.Contracts.AddAsync(contract);

            return contract;
        }

        private async Task<Company> AddOrGetCompanyAsync(string companyName)
        {
            var company = await _dbContext.Companies.FirstOrDefaultAsync(c => c.Name.ToUpper().Equals(companyName.ToUpper()));
            if (company is not null) return company;

            company = new Company { Name = companyName };
            await _dbContext.Companies.AddAsync(company);

            return company;
        }
    }
}
