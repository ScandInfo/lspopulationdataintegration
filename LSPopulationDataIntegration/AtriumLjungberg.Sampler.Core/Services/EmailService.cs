﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtriumLjungberg.Sampler.Core.Data;
using AtriumLjungberg.Sampler.Core.Entities;
using Mandrill;
using Mandrill.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace AtriumLjungberg.Sampler.Core.Services
{
    public class EmailService : IEmailService
    {
        private readonly ALDbContext _dbContext;
        private readonly NebuContext _nebuContext;
        private readonly IMandrillMessagesApi _messageApi;
        private readonly IConfiguration _config;
        private readonly ILogger<EmailService> _logger;

        public EmailService(ALDbContext dbContext, 
            NebuContext nebuContext,
            IMandrillMessagesApi messageApi,
            IConfiguration configuration,
            ILogger<EmailService> logger)
        {
            _dbContext = dbContext;
            _nebuContext = nebuContext;
            _messageApi = messageApi;
            _config = configuration;
            _logger = logger;
        }


        public async Task CreateMailQueueItems()
        {
            await CreateMailQueueItemsForSurveysAsync();
            await CreateMailQueueItemsForRemindersAsync();
        }

        private async Task SendEmailWithTemplateAsync(MailQueueItem mailItem)
        {
            var templateName = GetTemplateNameFromMailType(mailItem.MailQueueItemType);
            var fromEmailAddress = _config.GetValue<string>("MandrillSettings:FromAddress");

            var message = new MandrillMessage();

            message.FromEmail = fromEmailAddress;
            message.Subaccount = "AtriumLjungbergLokal";
            message.AddTo(mailItem.Email);
            message.AddGlobalMergeVars("StreetAddress", mailItem.StreetAddress);
            message.AddGlobalMergeVars("RespondentId", mailItem.RespondentId.ToString());
            message.AddGlobalMergeVars("Password", mailItem.RespondentPassword.ToString());
            await _messageApi.SendTemplateAsync(message, templateName);

            mailItem.IsHandled = true;

            var nebuSample = await _nebuContext.Samples.FirstOrDefaultAsync(s => s.Code == mailItem.RespondentId);
            nebuSample.Sent = "T";
            nebuSample.SentCount++;
            nebuSample.SentDateTime ??= DateTime.Now;

            await _nebuContext.SaveChangesAsync();

            _logger.LogInformation($"Email sent to {mailItem.Email}");
        }

        private string GetTemplateNameFromMailType(MailType mailType) => mailType switch
        {
            MailType.Survey => "atriumljungberglokal",
            MailType.Reminder => "atriumljungberglokalreminder",
            _ => throw new FormatException("Mail type does not match any template.")
        };

        private async Task CreateMailQueueItemsForSurveysAsync()
        {
            var newSamples = await _dbContext.Samples
                .Where(s => s.IsHandled == false)
                .ToListAsync();

            foreach (var sample in newSamples)
            {
                await _dbContext.MailQueueItems.AddAsync(
                    new MailQueueItem
                    {
                        Email = sample.Email,
                        SampleId = sample.SampleId,
                        MailQueueItemType = MailType.Survey,
                        SendMailAfter = DateTime.Now,
                        IsHandled = false,
                        RespondentId = sample.RespondentId.Value,
                        RespondentPassword = sample.RespondentPassword,
                        StreetAddress = sample.StreetAddress
                    }
                );
                sample.IsHandled = true;
            }

            await _dbContext.SaveChangesAsync();
        }

        private async Task CreateMailQueueItemsForRemindersAsync()
        {
            var reminderSamples = await _nebuContext.Samples.Include(f => f.FillingN)
                .Where(s => s.Status != SampleStatus.Completed &&
                    (s.SentCount == 1 && DateTime.Now.Date >= s.SentDateTime.Value.Date.AddDays(3) || s.SentCount == 2 && DateTime.Now.Date >= s.SentDateTime.Value.Date.AddDays(7)))
                .ToListAsync();

            foreach (var nebuSample in reminderSamples)
            {
                await _dbContext.MailQueueItems.AddAsync(
                    new MailQueueItem
                    {
                        Email = nebuSample.EMail,
                        SampleId = nebuSample.FillingN.SampleId,
                        MailQueueItemType = MailType.Reminder,
                        StreetAddress = nebuSample.FillingN.Adress,
                        RespondentId = nebuSample.Code,
                        RespondentPassword = Convert.ToInt32(nebuSample.Password1),
                        SendMailAfter = DateTime.Now,
                        IsHandled = false
                    });
            }

            await _dbContext.SaveChangesAsync();
            await _nebuContext.SaveChangesAsync();
        }

        public async Task ProcessMailQueueItems()
        {
            var mailQueueItems = await _dbContext.MailQueueItems.Where(mqi => mqi.IsHandled == false 
                                                                        && mqi.SendMailAfter <= DateTime.Now).ToListAsync();
            foreach (var mqi in mailQueueItems)
            {
                await SendEmailWithTemplateAsync(mqi);
                await _dbContext.SaveChangesAsync();
            }

        }
    }
}