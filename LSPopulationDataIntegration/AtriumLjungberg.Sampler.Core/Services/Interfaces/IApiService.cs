﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Services
{
    public interface IApiService<T> where T : class 
    {
        Task<List<T>> GetSamplesAsync();
    }
}