﻿using LS.PopulationData.Core.DTO;
using LS.PopulationData.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.PopulationData.Core.Services
{
    public interface IContractService
    {
        Task UpsertSamplesAsync(List<APIModelDTO> samples);
    }
}
