﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Services
{
    public interface IEmailService
    {
        Task CreateMailQueueItems();
        Task ProcessMailQueueItems();
    }
}
