﻿using AtriumLjungberg.Sampler.Core.Data;
using AtriumLjungberg.Sampler.Core.Entities;
using LS.PopulationData.Core.DTO;
using LS.PopulationData.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Microsoft.Extensions.Logging;

namespace AtriumLjungberg.Sampler.Core.Services
{
    public class ALApiService : IApiService<APIModelDTO>
    {
        private IRestClient _client;
        private IConfiguration _configuration;
        private readonly ILogger<ALApiService> _logger;

        public ALApiService(IConfiguration configuration, ILogger<ALApiService> logger)
        {
            _configuration = configuration;
            _logger = logger;
            ConfigureRestClient();
        }

        public async Task<List<APIModelDTO>> GetSamplesAsync()
        {

            var request = new RestRequest(_configuration.GetValue<string>("EndpointRequestPath"), DataFormat.Json);

            var response = await _client.ExecuteAsync<List<APIModelDTO>>(request);

            var data = response.Data;

            return data;
        }



        private void ConfigureRestClient()
        {
            _client = new RestClient(_configuration.GetValue<string>("EndpointBaseURL"));
            _client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            ConfigureHeaders();
        }

        private void ConfigureHeaders()
        {
            _client.AddDefaultHeader("x-api-key", _configuration.GetValue<string>("API_KEY"));
            _client.AddDefaultHeader("Accept", "*/*");
            _client.AddDefaultHeader("Content-Type", "application/json");
        }
    }
}
