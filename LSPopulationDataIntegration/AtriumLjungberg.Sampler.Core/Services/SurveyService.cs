﻿using AtriumLjungberg.Sampler.Core.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtriumLjungberg.Sampler.Core.Entities;
using AtriumLjungberg.Sampler.Core.Helpers;
using LS.PopulationData.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace AtriumLjungberg.Sampler.Core.Services
{
    public class SurveyService : ISurveyService
    {
        private ALDbContext _dbContext;
        private readonly IConfiguration _config;
        private readonly ILogger<SurveyService> _logger;
        private readonly NebuContext _nebuContext;
        private readonly int _surveyWeeklyThreshold;

        public SurveyService(
            ALDbContext dbContext, 
            NebuContext nebuContext,
            IConfiguration config, 
            ILogger<SurveyService> logger)
        {
            _dbContext = dbContext;
            _config = config;
            _logger = logger;
            _nebuContext = nebuContext;
            _surveyWeeklyThreshold = _config.GetValue<int>("SurveyThreshold");
        }


        public async Task SelectSamplesForSurveyAsync()
        {
            var firstDayOfWeek = GetfirstDayOfWeekAsDateTime(DateTime.Now, DateTime.Now.DayOfWeek);
            var samplesThisWeek = _dbContext.Samples
                .Count(s => s.SampledDate.Value.Date >= firstDayOfWeek);

            // Calculate how many emails to send this week.
            // If number of contracts increases, the amount sent should increase too.
            int numberOfContracts = GetNumberOfContracts();
            int weeklyQuota = numberOfContracts / 52;
            int amountToPick = weeklyQuota - samplesThisWeek;

            // If a person has gotten a survey within 305 days (10 months)
            // from last they got a survey they end up in this list.
            var quarantineSamples = await _dbContext.Samples
                .Where(s => s.SampledDate >= DateTime.Now.Date.AddDays(-305))
                .ToListAsync();

            var contracts = await _dbContext.Contracts
                .Include(rp => rp.ResponsiblePersons)
                .Where(c =>
                    c.Status.Equals("Aktuell") &&
                    c.FromDate <= DateTime.Now &&
                    c.ToDate >= DateTime.Now &&
                    c.ResponsiblePersons.Count > 0
                )
                .ToListAsync();

            // get contracts where the responsible person
            // isn't in the quarantine list. Order by random number.
            contracts = contracts.Where(c => !c.ResponsiblePersons.Any(r => quarantineSamples.Any(qe =>
                    qe.Email == r.ResponsiblePersonEmail && qe.StreetAddress == r.Contract.StreetAddress)))
                .OrderBy(r => Guid.NewGuid())
                .Take(amountToPick)
                .ToList();

            var responsiblePersons = contracts
                .SelectMany(r =>
                    r.ResponsiblePersons.Where(r => r.HasNKIFlagEnabled).DistinctBy(rp => rp.ResponsiblePersonEmail))
                .ToList();

            var samples = responsiblePersons.Select(r =>
                new Sample
                {
                    Email = r.ResponsiblePersonEmail,
                    ObjectNo = r.Contract.ObjectNo,
                    BusinessArea = r.Contract.PropertyBusinessArea,
                    PropertyId = r.Contract.PropertyId,
                    StreetAddress = r.Contract.StreetAddress,
                    SampledDate = DateTime.Now.Date
                }).ToList();

            await _dbContext.Samples.AddRangeAsync(samples);
            await _dbContext.SaveChangesAsync();

            await AddNebuSamplesAsync(samples);
        }

        /// <summary>
        /// Get current number of possible respondents that still has an active contract.
        /// group by street address and email to get a distinct list.
        /// </summary>
        /// <returns></returns>
        private int GetNumberOfContracts()
        {
            return (
                from c in _dbContext.Contracts
                where c.Status == "Aktuell" && c.FromDate <= DateTime.Now && c.ToDate >= DateTime.Now
                select c).Count();
        }

        private static DateTime GetfirstDayOfWeekAsDateTime(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public async Task<bool> IsWeeklyQuotaFilledAsync()
        {
            var firstDayOfWeek = GetfirstDayOfWeekAsDateTime(DateTime.Now, DayOfWeek.Monday);
            var samplesThisWeek = _dbContext.Samples
                .Count(s => s.SampledDate.Value.Date >= firstDayOfWeek);

            // Calculate how many emails to send this week.
            // If number of contracts increases, the amount sent should increase too.
            int numberOfContracts = GetNumberOfContracts();
            int weeklyQuota = numberOfContracts / 52;
            
            return await Task.FromResult(samplesThisWeek >= weeklyQuota);
        }

        private async Task<int> GetHighestRespondentIdAsync()
        {
            var respondentId =  await _nebuContext.Set<NebuSample>().MaxAsync(mqi => (int?)mqi.Code) ?? 999;
            respondentId += 1;
            return respondentId;
        }

        private async Task AddNebuSamplesAsync(List<Sample> samples)
        {
            foreach (var sample in samples)
            {
                var respondentId = await GetHighestRespondentIdAsync();
                var respondentPassword = await GenerateRespondentPasswordAsync();
                await _nebuContext.Samples.AddAsync(
                    new NebuSample
                {
                    Code = respondentId,
                    EMail = sample.Email,
                    Password1 = respondentPassword.ToString(),
                    Received = "F",
                    Responded = "F",
                    Sent = "T",
                    SentCount = 0,
                    Status = SampleStatus.NotStarted,
                    Started = "F",
                    FillingN = new NebuFillingN { Adress = sample.StreetAddress, CallCode = respondentId, SampleId = sample.SampleId}
                });

                sample.RespondentId = respondentId;
                sample.RespondentPassword = respondentPassword;
                await _dbContext.SaveChangesAsync();
                await _nebuContext.SaveChangesAsync();
            }


        }

        private Task<int> GenerateRespondentPasswordAsync()
        {
            var randomizer = new Random();
            return Task.FromResult(randomizer.Next(100000, 999999));
        }
    }



    public interface ISurveyService
    {
        Task SelectSamplesForSurveyAsync();
        Task<bool> IsWeeklyQuotaFilledAsync();
    }
}
