﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LS.PopulationData.Core.Entities;

namespace LS.PopulationData.Core.DTO
{
    public class APIModelDTO
    {
        public string object_no { get; set; }
        public string object_address { get; set; }
        public string company_name { get; set; }
        public string person_name { get; set; }
        public string person_email { get; set; }
        public List<string> person_sendouts { get; set; } = new();
        public string property_postalcity { get; set; }
        public string property_propertyid { get; set; }
        public DateTime contract_todate { get; set; }
        public DateTime contract_fromdate { get; set; }
        public string contract_objecttype { get; set; }
        public string contract_contractstatus { get; set; }
        public string businessarea_area { get; set; }
    }
}
