﻿using AtriumLjungberg.Sampler.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.PopulationData.Core.Entities
{
    public class Contract
    {
        public int ContractId { get; set; }
        public string ObjectNo { get; set; } 
        public string StreetAddress { get; set; }
        public string PostalCity { get; set; }
        public string PropertyBusinessArea { get; set; }
        public string PropertyId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ObjectType { get; set; }
        public string Status { get; set; }
        public virtual Company Company { get; set; }
        public int CompanyId { get; set; }
        public virtual List<ResponsiblePerson> ResponsiblePersons { get; set; } = new();
        public DateTime? LastSurveySendOut { get; set; } = null;
    }
}
