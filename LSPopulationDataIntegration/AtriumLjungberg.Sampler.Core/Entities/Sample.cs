﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Entities
{
    public class Sample
    {
        public int SampleId { get; set; }
        public int? RespondentId { get; set; } = null;
        public int RespondentPassword { get; set; }
        public string ObjectNo { get; set; }
        public string Email { get; set; }
        public string BusinessArea { get; set; }
        public string PropertyId { get; set; }
        public string StreetAddress { get; set; }
        public DateTime? SampledDate { get; set; } = null;
        public bool IsHandled { get; set; } = false;
    }
}
