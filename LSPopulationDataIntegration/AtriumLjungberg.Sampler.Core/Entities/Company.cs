﻿using LS.PopulationData.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Entities
{
    public class Company
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
    }
}
