﻿using LS.PopulationData.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Entities
{
    public class ResponsiblePerson
    {
        public int ResponsiblePersonId { get; set; }
        public string ResponsiblePersonName { get; set; }
        public string ResponsiblePersonEmail { get; set; }
        public bool HasNKIFlagEnabled { get; set; }
        public virtual Contract Contract { get; set; }
        public int ContractId { get; set; }

    }
}
