﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Entities
{
    public class MailQueueItem
    {
        public int MailQueueItemId { get; set; }
        public int RespondentId { get; set; }
        public int RespondentPassword { get; set; }
        public string StreetAddress { get; set; }
        public string Email { get; set; }
        public MailType MailQueueItemType { get; set; }
        public virtual Sample Sample { get; set; }
        public int SampleId { get; set; }
        public DateTime SendMailAfter { get; set; }
        public bool IsHandled { get; set; } = false;

    }

    public enum MailType
    {
        Survey = 1,
        Reminder = 2
    }
}
