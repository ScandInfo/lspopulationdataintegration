﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Entities
{
    public class NebuFillingN
    {
        public int CallCode { get; set; }
        public string Adress { get; set; }
        public int SampleId { get; set; }
    }
}
