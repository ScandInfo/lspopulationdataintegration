﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtriumLjungberg.Sampler.Core.Entities
{
    [Table("Sample")]
    public class NebuSample
    {
        private int _status;
        public int Code { get; set; }
        public string EMail { get; set; }
        public string Password1 { get; set; }
        public string Sent { get; set; }
        public string Received { get; set; }
        public int SentCount { get; set; }
        public string Started { get; set; }
        public string Responded { get; set; }
        public DateTime? SentDateTime { get; set; } = null;
        public SampleStatus Status
        {
            get => (SampleStatus)_status;
            set => _status = (int)value;
        }
        [ForeignKey("Code")]
        public virtual NebuFillingN FillingN { get; set; }
    }

    public enum SampleStatus
    {
        NotStarted = 1,
        Active = 2,
        Completed = 4
    }
}
