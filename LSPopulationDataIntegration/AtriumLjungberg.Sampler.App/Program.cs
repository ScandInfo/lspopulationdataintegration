﻿using System;
using LS.PopulationData.Core.DTO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading.Tasks;
using LS.PopulationData.Core.Services;
using AtriumLjungberg.Sampler.Core.Services;
using AtriumLjungberg.Sampler.Core.Data;
using Mandrill;
using Microsoft.EntityFrameworkCore;

namespace AtriumLjungberg.App
{
    class Program
    {
        private static IServiceProvider _services;
        private static ILogger<Program> _logger;
        private static IConfigurationRoot _configuration;
        private static IContractService _contractService;
        private static IApiService<APIModelDTO> _apiService;
        private static ISurveyService _surveyService;
        private static IEmailService _emailService;

        static async Task Main(string[] args)
        {
            CreateServiceProvider();

            if (IsDaySunday())
            {
                var samples = await _apiService.GetSamplesAsync();

                _logger.LogInformation($"Writing {samples.Count} samples to database...");
                await _contractService.UpsertSamplesAsync(samples);
            }

            if (IsDayBetweenMondayAndFriday())
            {
                var isWeeklyQuotaFilled = await _surveyService.IsWeeklyQuotaFilledAsync();
                if (!isWeeklyQuotaFilled)
                {
                    _logger.LogInformation($"Processing samples for survey...");
                    await _surveyService.SelectSamplesForSurveyAsync();
                }
                _logger.LogInformation("Creating mail queue items...");
                await _emailService.CreateMailQueueItems();
                _logger.LogInformation("Processing mail queue items...");
                await _emailService.ProcessMailQueueItems();
            }

            _logger.LogInformation("Done");
        }

        private static void CreateServiceProvider()
        {
            var serviceCollection = new ServiceCollection();
            
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            
            //Uncomment line below if you want to run in debug mode with production-appsettings.
            //environmentName = "";
            
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile($"appsettings.json", true, true)
                // Uncommented line below because program used AL-test-api-url instead of the actual one with the real data.
                //.AddJsonFile($"appsettings.{environmentName}.json", true, true)
                .AddEnvironmentVariables()
                .Build();

            var mandrillApi = new MandrillApi(_configuration.GetValue<string>("MandrillSettings:MandrillApiKey"));

            serviceCollection.AddLogging(configure => configure.AddConsole())
                .AddDbContext<ALDbContext>(options => options.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"), x => x.MigrationsAssembly("AtriumLjungberg.Sampler.App")))
                .AddDbContext<NebuContext>(options => options.UseSqlServer(_configuration.GetConnectionString("NebuConnectionString")))
                .AddSingleton<IConfiguration>(_configuration)
                .AddTransient<IApiService<APIModelDTO>, ALApiService>()
                .AddTransient<ISurveyService, SurveyService>()
                .AddTransient<IContractService, ContractService>()
                .AddTransient<IEmailService, EmailService>()
                .AddSingleton(mandrillApi.Messages);



            _services = serviceCollection.BuildServiceProvider();

            _logger = _services.GetService<ILogger<Program>>();
            _contractService = _services.GetService<IContractService>();
            _apiService = _services.GetService<IApiService<APIModelDTO>>();
            _surveyService = _services.GetService<ISurveyService>();
            _emailService = _services.GetService<IEmailService>();
        }

        private static bool IsDaySunday() => DateTime.Now.DayOfWeek switch
        {
            DayOfWeek.Sunday => true,
            _ => false
        };

        private static bool IsDayBetweenMondayAndFriday() => DateTime.Now.DayOfWeek switch
        {
            DayOfWeek.Sunday or DayOfWeek.Saturday => false,
            _ => true
        };
    }
}
